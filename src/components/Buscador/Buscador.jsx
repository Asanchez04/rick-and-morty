import '../../App.css';

export function Buscador({ valor, setValor }) {
  return (
    <div className='my-4 d-flex justify-content-end'>
      <div className='mb-3 col-4'>
        <input
          value={valor}
          onChange={(evento) => setValor(evento.target.value)}
          className='form-control'
          placeholder='Buscar Personaje...'
        />
      </div>
    </div>
  );
}
