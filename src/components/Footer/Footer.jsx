import '../../App.css';
export function Footer() {
  return (
    <div
      className=' navbar-dark bg dark border border-light border rounded-3'
      style={{ backgroundColor: 'black' }}
    >
      <p
        className='text-center text-white py-2 mb-0'
        style={{ fontSize: '25px' }}
      >
        Proyecto creado por Alan Sanchez CAR-IV
      </p>
      <a
        className='justify-content-end px-3'
        style={{ fontSize: '15px' }}
        href='https://www.facebook.com/lsvtechcolombia11'
      >
        LSV-TECH
      </a>
    </div>
  );
}
