export function EpisodioItem({ name, air_date, episode }) {
  return (
    <div className='col-lg-3'>
      <div
        className='card text-dark bg-light mb-4  '
        style={{ maxWidth: '18rem' }}
      >
        <div className='card-header'>{air_date}</div>
        <div className='card-body'>
          <h5 className='card-title'>{name}</h5>
          <p className='card-text'>{episode}</p>
        </div>
      </div>
    </div>
  );
}
