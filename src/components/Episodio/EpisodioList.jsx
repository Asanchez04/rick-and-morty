import { EpisodioItem } from "./EpisodioItem"
export function EpisodioList({ episodios }) {
    return (
      <div className='row px-5'>
            {episodios.map((episodio) => (
                <EpisodioItem key={episodio.data.id} {...episodio.data}/>
        ))}
      </div>
    );
}