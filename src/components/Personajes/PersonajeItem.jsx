import '../../App.css';
import {Link} from 'react-router-dom'
function getEstilosStatus(status) {
  let color = 'greenyellow';

  if (status === 'unknown') {
    color = 'gray';
  }

  if (status === 'Dead') {
    color = 'red';
  }

  const estiloCirculo = {
    backgroundColor: color,
  };
  return estiloCirculo;
}
export function PersonajeItem({
  id,
  name,
  status,
  species,
  location,
  image,
  origin,
}) {
  return (
    <div className='col-lg-4 '>
      <Link
        to={`/personaje/${id}`}
        style={{ textDecoration: 'none', color: 'initial' }}
      >
        <div className='card mb-3' id='itemcard' style={{
          transition: 'box-shadow 0.3s' ,
          maxWidht: '360px',
          display: 'block',
          borderStyle: 'solid',
          //borderColor: 'blue',
          backgroundColor: 'lavender',
          cursor: 'pointer',
        }}>
        <div className='row g-0'>
          <div className='col-md-4'>
            <img
              style={{ height: '100%' ,objectFit: 'cover' }}
              src={image}
              className='img-fluid rounded-start'
              alt={name}
            />
          </div>
          <div className='col-md-8'>
            <div className='card-body'>
                <h5 className='card-title mb-0' style={{ inline: 'block'}}>{name}</h5>
              <p>
                <span
                  className='vivo-muerto'
                  style={getEstilosStatus(status)}
                ></span>
                {status} - {species}
              </p>
              <p className='mb-0 text-muted'>Last know location:</p>
              <p >{location?.name}</p>

              <p className='mb-0 text-muted'>Origin:</p>
              <p> {origin?.name} </p>
            </div>
          </div>
        </div>
      </div>
      </Link>
    </div>
  );
}
