import Pagination from 'react-js-pagination';
import { PersonajeItem } from './PersonajeItem';
import { useState, useEffect } from 'react';
import axios from 'axios';

export function PersonajeList({buscador}) {
  let [personajes, setPersonajes] = useState(null);
  let [pagina, setPagina] = useState();

  let personajesFiltrados = personajes?.results;

  if (buscador && personajes) {
    personajesFiltrados = personajes.results.filter((personaje) => {
      let nombrePersonajeMinuscula = personaje.name.toLowerCase();
      let buscadorMinuscula = buscador.toLowerCase();
      return nombrePersonajeMinuscula.includes(buscadorMinuscula);
    })
  }


  useEffect(() => {
    axios
      .get(`https://rickandmortyapi.com/api/character/?page=${pagina}`)
      .then((respuesta) => {
        setPersonajes(respuesta.data);
      });
  }, [pagina]);

  function handlePageChange(pageNumber) {
    setPagina(pageNumber);
  }


  return (
    <div className='row py-3' style={{}}>
      {personajesFiltrados
        ? personajesFiltrados.map((elemento) => {
            return <PersonajeItem key={elemento.id} {...elemento} />;
          })
        : 'Cargando...'}

      <div className='d-flex justify-content-center'>
        <Pagination
          itemClass='page-item'
          linkClass='page-link'
          activePage={pagina}
          itemsCountPerPage={20}
          totalItemsCount={826}
          pageRangeDisplayed={7}
          onChange={handlePageChange}
        />
      </div>
    </div>
  );
}
