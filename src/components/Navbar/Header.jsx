import { Link } from 'react-router-dom';

export function Header() {
  return (
    <nav
      className='navbar navbar-expand-lg navbar-dark  sticky-top' id='navbar-hero'
    >
      <div className='container-fluid' >
        <Link className='navbar-brand' to='/'>
          Rick and Morty
        </Link>
        
        <div className='collapse navbar-collapse'>
  
        </div>
      </div>
    </nav>
  );
}
