import bannerImg from '../../img/RickandMorty.jpg'
import '../../App.css'
export function Banner() {
  return (
    <div className='text-white mb-3' >
      <img src={bannerImg}
        className='card-img'
        alt='banner'
      />
    </div>
  );
}
