import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { Routes, Route } from 'react-router-dom';
import { Home } from './Pages/Home';
import { Personaje } from './Pages/Personaje';
import { Header } from './components/Navbar/Header'
import { Footer } from './components/Footer/Footer'

function App() {
  return (
    <div className='App'>
      <Header />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/Personaje/:personajeId' element={<Personaje />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
