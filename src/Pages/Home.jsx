import { Banner } from '../components/Navbar/Banner';
import { Buscador } from '../components/Buscador/Buscador';
import { PersonajeList } from '../components/Personajes/PersonajeList';
import { useState, useEffect } from 'react'
export function Home() {
 
  let [buscar, setBuscar] = useState('');

  
  console.log(buscar)
  return (
    <div>
      <Banner />
      <div className='px-3'>
        <h1 style={{ color: 'white' }} className='py-4'>
          Personajes
        </h1>
        <Buscador valor={buscar} setValor={setBuscar}/>
        <PersonajeList buscador={buscar}/>
      </div>
    </div>
  );
}
