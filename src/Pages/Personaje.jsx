import { useParams } from 'react-router-dom';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { BannerPersonaje } from '../components/Personajes/BannerPersonaje';
import { EpisodioList } from '../components/Episodio/EpisodioList';
export function Personaje() {
  let { personajeId } = useParams();
  let [personaje, setPersonaje] = useState(null);
  let [episodios, setEpisodios] = useState(null);

  useEffect(() => {
    axios
      .get(`https://rickandmortyapi.com/api/character/${personajeId}`)
      .then((res) => {
        setPersonaje(res.data);
      });
  }, []);

  useEffect(() => {
    if (personaje) {
      let peticionEpisodios = personaje.episode.map((episodio) => {
        return axios.get(episodio);
      });
      Promise.all(peticionEpisodios).then((res) => {
        setEpisodios(res);
      });
    }
  }, [personaje]);
  console.log(episodios);

  return (
    <div>
      <BannerPersonaje {...personaje} />

      <h1 className='py-4 mx-5' style={{ color: 'white' }}>
        Episodios
      </h1>

      {episodios ? (
        <EpisodioList episodios={episodios} />
      ) : (
        <div>cargando episodios...</div>
      )}
    </div>
  );
}
